# Ansible playbook GitLab

Spin up GitLab and a GitLab runner on AWS.

## Requirements

Be sure to have the variables set:

```bash
export AWS_ACCESS_KEY_ID="abc"
export AWS_SECRET_ACCESS_KEY="bcd"
```

## Setup

Download (or update) the used Ansible roles locally.

```shell
ansible-galaxy install -r roles/requirements.yml -f
```

Also read the steps required for Terraform in [`terraform/README.md`](terraform/README.md).

## Create

```shell
ssh-add ../files/id_rsa
./playbook
```

### Special considerations

1. On the first run, there is not `gitlab_runner_token` known. `gitlab_runner_token` needs to be commented in [`ansible/inventory/group_vars/runners/gitlab_runner.yml`](group_vars/runner/gitlab_runner.yml).

## Backup and restore

There is a `backup.yml` playbook to backup an existing GitLab instance. This can backup to an "empty" GitLab installation.
There is also a `restore.yml` playbook that can be used to restore a backup, created with `backup.yml`.

Files are saved locally, on the Ansible controller. Make sure there is sufficient space.

## CI

Simply push your changes, which will trigger:

- init
- validate
- plan

Next steps are manually started:

- apply
- play
- destroy
