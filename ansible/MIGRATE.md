# Migrate

You can mimic a migration using this code.

1. Start the `playbook.yml` to configure a fresh installation of GitLab.
2. Make a `backup.yml`.
3. Destroy the GitLab server: `cd terraform ; terraform destroy -target aws_instance.gitlab -target aws_ebs_volume.data -target aws_ebs_volume.backup`.
4. Recreate the GitLab server: `terraform apply`.
5. Install a fresh copy of GitLab: `cd ../ ; ./playbook.yml --limit localhost:gitlab`.
6. Restore a backup: `restore.yml`
