# Files

Place configurations files with a specific name here. You need to tell Ansible what server gets which file. This can be done in:

1. `host_vars/{{ hostname }}/gitlab.yml`
2. `group_vars/{{ groupname }}/gitlab.yml`
