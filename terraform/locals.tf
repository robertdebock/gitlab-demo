locals {
  _maximum_iops = {
    gp2 = 16000
    gp3 = 16000
    io2 = 256000
    sc1 = 250
    st1 = 500
  }
  maximum_iops = local._maximum_iops[var.data_volume_type]

  _maximum_throughput = {
    gp2 = 250
    gp3 = 1000
    io2 = null
    st1 = 500
    sc1 = 250
  }
  maximum_throughput = local._maximum_throughput[var.data_volume_type]
}
