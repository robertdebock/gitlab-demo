variable "gitlab_instance_type" {
  type        = string
  default     = "c5.2xlarge"
  description = "The instance type to run GitLab on."
  validation {
    condition     = contains(["c5.xlarge", "c5.2xlarge"], var.gitlab_instance_type)
    error_message = "Please use \"c5.xlarge\" or \"c5.2xlarge\"."
  }
}

variable "os_volume_size" {
  type        = number
  default     = 16
  description = "The size (in GB) of the root block device. Minimum is \"8\"."
  validation {
    condition     = var.os_volume_size >= 8
    error_message = "Please use \"8\" or more."
  }
}

variable "data_volume_size" {
  type        = number
  default     = 16
  description = "The size (in GB) of the ebs block device. Minimum is \"2\"."
  validation {
    condition     = var.data_volume_size >= 2
    error_message = "Please use \"2\" or more."
  }
}

variable "data_volume_type" {
  type        = string
  default     = "gp3"
  description = "The type of volume for the ebs device."
  validation {
    condition     = contains(["gp2", "gp3", "io2", "sc1", "st1"], var.data_volume_type)
    error_message = "Please use one of \"gp2\", \"gp3\", \"io2\", \"sc1\" or \"st1\"."
  }
}

variable "backup_volume_size" {
  type        = number
  default     = 16
  description = "The size of the volume to store backups on."
  validation {
    condition     = var.backup_volume_size >= 2
    error_message = "Please use \"2\" or more."
  }
}
