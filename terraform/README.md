# Terraform

## Download Terraform providers

```shell
terraform init
```

## Apply

```shell
terraform apply
```

## Cleanup

```shell
terraform destroy
```
