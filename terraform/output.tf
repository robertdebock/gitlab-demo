output "gitlab" {
  value = aws_route53_record.gitlab.fqdn
}

output "runner" {
  value = aws_route53_record.runner.*.fqdn
}
