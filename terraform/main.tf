data "aws_ami" "default" {
    most_recent = true
    filter {
        name   = "name"
        values = ["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"]
    }
    /* filter {
        name = "virtualization - type"
        values = ["hvm"]
    } */
    owners = ["099720109477"]
}

resource "tls_private_key" "default" {
  algorithm = "RSA"
}

resource "local_file" "id_rsa" {
  filename        = "../files/id_rsa"
  content         = tls_private_key.default.private_key_pem
  file_permission = "0600"
}

resource "aws_key_pair" "default" {
  key_name   = "gitlab"
  public_key = tls_private_key.default.public_key_openssh
}

resource "aws_security_group" "allow_internet" {
  name        = "allow_internet"
  description = "Allow internet outbound traffic"
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_security_group" "allow_icmp" {
  name        = "allow_icmp"
  description = "Allow ICMP traffic"
  ingress {
    description = "ICMP"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow SSH inbound traffic"
  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_web" {
  name        = "allow_web"
  description = "Allow web inbound traffic"
  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_availability_zones" "default" {
  state = "available"
}

resource "aws_instance" "gitlab" {
  ami               = data.aws_ami.default.id
  instance_type     = var.gitlab_instance_type
  availability_zone = data.aws_availability_zones.default.names[1]
  key_name          = aws_key_pair.default.key_name
  security_groups   = [aws_security_group.allow_ssh.name, aws_security_group.allow_internet.name, aws_security_group.allow_icmp.name, aws_security_group.allow_web.name]
  root_block_device {
    volume_size = var.os_volume_size
    volume_type = "gp3"
  }
}

resource "aws_ebs_volume" "data" {
  availability_zone = data.aws_availability_zones.default.names[1]
  iops              = max(min(var.data_volume_size * 500, local.maximum_iops), 3000)
  size              = var.data_volume_size
  type              = var.data_volume_type
  throughput        = var.data_volume_type == "io2" ? 0 : min(var.data_volume_size * 500 * 0.25, local.maximum_throughput)
}

resource "aws_volume_attachment" "data" {
  device_name = "/dev/sdf"
  volume_id   = aws_ebs_volume.data.id
  instance_id = aws_instance.gitlab.id
}

resource "aws_ebs_volume" "backup" {
  availability_zone = data.aws_availability_zones.default.names[1]
  size              = var.backup_volume_size
}

resource "aws_volume_attachment" "backup" {
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.backup.id
  instance_id = aws_instance.gitlab.id
}

resource "aws_instance" "runner" {
  count           = 1
  ami             = data.aws_ami.default.id
  instance_type   = "t2.medium"
  key_name        = aws_key_pair.default.key_name
  security_groups = [aws_security_group.allow_ssh.name, aws_security_group.allow_internet.name, aws_security_group.allow_icmp.name, aws_security_group.allow_web.name]
}

data "aws_route53_zone" "default" {
  name         = "aws.adfinis.cloud"
  private_zone = false
}

resource "aws_route53_record" "gitlab" {
  zone_id = data.aws_route53_zone.default.id
  name    = "gitlab.aws.adfinis.cloud"
  type    = "A"
  ttl     = 300
  records = [aws_instance.gitlab.public_ip]
}

resource "aws_route53_record" "runner" {
  count   = length(aws_instance.runner)
  zone_id = data.aws_route53_zone.default.id
  name    = "runner-${count.index}.aws.adfinis.cloud"
  type    = "A"
  ttl     = 300
  records = [aws_instance.runner[count.index].public_ip]
}

resource "local_file" "all" {
  content              = templatefile("templates/all.tpl", { hosts = concat([aws_route53_record.gitlab.fqdn], aws_route53_record.runner.*.fqdn) })
  filename             = "${path.module}/../ansible/inventory/all"
  directory_permission = "0755"
  file_permission      = "0644"
}

resource "local_file" "gitlab" {
  content              = templatefile("templates/gitlab.tpl", { hosts = [aws_route53_record.gitlab.fqdn] })
  filename             = "${path.module}/../ansible/inventory/gitlab"
  directory_permission = "0755"
  file_permission      = "0644"
}

resource "local_file" "runners" {
  content              = templatefile("templates/runners.tpl", { hosts = aws_route53_record.runner.*.fqdn })
  filename             = "${path.module}/../ansible/inventory/runners"
  directory_permission = "0755"
  file_permission      = "0644"
}
